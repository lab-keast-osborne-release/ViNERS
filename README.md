# ViNERS 1.2.0 

ViNERS (Visceral Nerve Ensemble Recording & Stimulation) is a neural interface model for simulating electrical stimulation and recording of spontaneous and electrically-evoked activity.

For detailed documentation, please check out [the wiki](https://gitlab.unimelb.edu.au/lab-keast-osborne-release/viners/-/wikis/home). If you have found ViNERS useful in your work, please cite *Computational modelling of nerve stimulation and recording with peripheral visceral neural interfaces*. Calvin D Eiber, *et al.*, 2021 [J. Neural Eng. *18* 066020](https://doi.org/10.1088/1741-2552/ac36e2)

## Changelog

- 1.2.0 Major upgrades across all modules to functionality and documentation.
- 1.1.3 add fix-tool for EIDORS sparse error 
- 1.1.2 corrected an issue with multi-fascicle nerve recordings (ENG and ECAP)
- 1.1.1 Upgrades to installer for cross-platform and cross-version stability 
- 1.0.0 Initial release accompanying the submission of [Computational modelling of nerve stimulation and recording with peripheral visceral neural interfaces](https://doi.org/10.1088/1741-2552/ac36e2)

